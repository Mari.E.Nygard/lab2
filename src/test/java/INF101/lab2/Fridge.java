package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	ArrayList<FridgeItem> Items;
	
	public Fridge() {
		Items = new ArrayList<FridgeItem>();
	}

	@Override
	public int nItemsInFridge() {
		// TODO Auto-generated method stub
		return Items.size();
	}

	@Override
	public int totalSize() {
		// TODO Auto-generated method stub
		return 20;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (nItemsInFridge() < totalSize()) {
			Items.add(item);
			return true;
		}
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void takeOut(FridgeItem item) {
		if (Items.contains(item)) {
			Items.remove(item);
		}
		else {
			throw new NoSuchElementException();
		}

		// TODO Auto-generated method stub

	}
	@Override
	public void emptyFridge() {
		Items.clear();
		// TODO Auto-generated method stub

	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> expiredFood = new ArrayList<>();
		
		for (int i=0; i < nItemsInFridge(); i++) {
			FridgeItem item = Items.get(i);
			if (item.hasExpired()) {
				expiredFood.add(item);
			}
		}
		for (FridgeItem expiredItem : expiredFood) {
			Items.remove(expiredItem);
		}
		
		return expiredFood;
			
	
		// TODO Auto-generated method stub
	}

}
